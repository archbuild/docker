FROM base/devel:2018.06.01

RUN curl -o /etc/pacman.d/mirrorlist "https://www.archlinux.org/mirrorlist/?country=FR&protocol=https&ip_version=4" \
  && sed -i 's/^#//' /etc/pacman.d/mirrorlist \
  && pacman -Syy

RUN useradd -ms /bin/bash builder \
  && echo "builder ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/90-sudo

RUN mkdir .gnupg
ADD gpg.conf .gnupg/gpg.conf
